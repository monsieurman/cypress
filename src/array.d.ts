interface Array<T> {
  /**
   * Map all elements matching a given condition.
   *
   * @param cond Condition the element must meet to be transformed.
   * @param callbackfn The transformation to apply to the corresponding elements.
   */
  mapWhere(
    cond: (value: T, index: number, array: T[]) => boolean,
    callbackfn: (value: T, index: number, array: T[]) => T
  ): T[]
}
