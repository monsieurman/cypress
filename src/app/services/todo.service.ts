import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Todo, TodoInput, TodoState } from '../models/todo';
import { ID } from '../models/id';
import v4 from 'uuid/v4';
import { tmpdir } from 'os';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private todos$ = new BehaviorSubject<Todo[]>([]);

  constructor() {
    this.todos$.next([{
      content: 'Do laundry',
      id: v4(),
      state: TodoState.Pending
    }]);
  }

  get todos(): Observable<Todo[]> {
    return this.todos$.asObservable();
  }

  addTodo(todo: TodoInput) {
    this.todos$.next([
      ...this.todos$.value,
      {
        id: v4(),
        content: todo.content,
        state: TodoState.Pending
      }
    ]);
  }

  removeTodo(id: ID) {
    this.todos$.next(
      this.todos$.value.filter(t => t.id !== id)
    );
  }

  toggleTodo(id: ID) {
    this.todos$.next(this.todos$.value.mapWhere(
      t => t.id === id,
      t => ({ ...t, state: (t.state === TodoState.Pending)
        ? TodoState.Completed
        : TodoState.Pending
      })
    ));
  }

  completeTodo(id: ID) {
    this.setTodoState(id, TodoState.Completed);
  }

  cancelTodo(id: ID) {
    this.setTodoState(id, TodoState.Cancelled);
  }

  undoTodo(id: ID) {
    this.setTodoState(id, TodoState.Pending);
  }

  private setTodoState(id: string, state: TodoState) {
    this.todos$.next(this.todos$.value.mapWhere(
      t => t.id === id,
      t => ({ ...t, state })
    ));
  }
}
