import { Component, Input, HostListener } from '@angular/core';
import { Todo, TodoState } from 'src/app/models/todo';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  template: `
    <wcs-checkbox [checked]="todo.state === states.Completed"></wcs-checkbox>
    <span>{{ todo.content }}</span>
    <wcs-button
        (click)="deleteTodo()"
        class="del-btn flex-right wcs-secondary"
        shape="square"
        mode="clear"
    >
      <i class="material-icons">delete</i>
    </wcs-button>
  `,
  styles: [`
    :host {
      display: flex;
      align-items: center;
      cursor: pointer;
    }
    wcs-checkbox {
      pointer-events: none;
    }
    :host(:hover) {
      color: var(--wcs-primary);
    }
    :host(:hover)>wcs-checkbox {
      --wcs-checkbox-border-color: var(--wcs-primary);
      --wcs-checkbox-text-color: var(--wcs-primary);
    }
    .del-btn:hover {
      --wcs-base: var(--wcs-red);
    }
  `]
})
export class TodoComponent {
  @Input()
  todo!: Todo;

  states = TodoState;

  constructor(
    private todoService: TodoService
  ) { }

  @HostListener('click')
  onClick() {
    this.todoService.toggleTodo(this.todo.id);
  }

  deleteTodo() {
    this.todoService.removeTodo(this.todo.id);
  }
}
