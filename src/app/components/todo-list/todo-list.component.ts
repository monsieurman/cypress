import { Component, Input, HostBinding } from '@angular/core';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-todo-list',
  template: `
    <app-todo *ngFor="let todo of todos" [todo]="todo"></app-todo>
  `,
  styles: []
})
export class TodoListComponent {
  @HostBinding('class') class = 'spacing-xs';

  @Input()
  todos!: Todo[] | null;
}
