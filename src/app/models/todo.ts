import { ID } from './id';

export enum TodoState {
  Pending = 'PENDING',
  Cancelled = 'CANCELLED',
  Completed = 'COMPLETED',
}

export interface Todo {
  content: string;
  id: ID;
  state: TodoState;
}

export interface TodoInput {
  content: string;
}
