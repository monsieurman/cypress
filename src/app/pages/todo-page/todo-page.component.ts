import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoService } from 'src/app/services/todo.service';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-todo-page',
  template: `
    <wcs-card>
      <wcs-card-body class="spacing-x">
        <!-- TODO: extract component -->
        <wcs-form-field>
          <input #input
            (change)="createTodo(input.value); input.value='';"
            class="full-width"
            placeholder="What needs to be done ?"
            type="text">
        </wcs-form-field>
        <app-todo-list *ngIf="(todos | async).length" [todos]="todos | async"></app-todo-list>
      </wcs-card-body>
    </wcs-card>
  `,
  styles: []
})
export class TodoPageComponent {
  todos!: Observable<Todo[]>;

  constructor(
    private todoService: TodoService
  ) {
    this.todos = todoService.todos;
  }

  createTodo(content: string) {
    if (content !== '') {
      this.todoService.addTodo({ content });
    }
  }
}
