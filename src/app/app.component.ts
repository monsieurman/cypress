import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <wcs-header>
      <h1 slot="title">
        Todo MVC
      </h1>
    </wcs-header>
    <main>
      <router-outlet></router-outlet>
    </main>
  `,
  styles: [`
    main {
      padding: 32px;
    }
  `]
})
export class AppComponent {
  title = 'todo-cypress-graphql';
}
