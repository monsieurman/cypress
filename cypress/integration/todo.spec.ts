describe('Todo list', () => {
  beforeEach(() => {
    cy.visit('localhost:4200');
  });

  it('Displays present todos', () => {
    cy.get('app-todo')
      .should('include.text', 'Do laundry');
  });

  it('Let add a todo', () => {
    cy.get('input').type('Do the laundry ! {Enter}');

    cy.get('app-todo')
      .should('have.length', 2);
  });

  it('Let complete a todo', () => {
    cy.get('app-todo')
      .click();
  });

  it('Let delete a todo', () => {
    cy.get('app-todo>.del-btn')
      .click();

    cy.get('app-todo')
      .should('not.exist');
  });
});
